package jse29;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Сервисный класс
 */
public class NumberService {
    /**
     * Строку в Integer
     * @param arg строка
     * @return Integer
     * @throws IllegalArgumentException если по строке не удалось получить число
     */
    public Integer convertStringToInteger(String arg) throws IllegalArgumentException {
        int integerArg;
        try {
            integerArg = Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("По строке " + arg + " не удалось определить число");
        }
        return integerArg;
    }
    /**
     * Строку в Long
     * @param arg строка
     * @return Long
     * @throws IllegalArgumentException если по строке не удалось получить число
     */
    public Long convertStringToLong(String arg) throws IllegalArgumentException {
        long longArg;
        try {
            longArg = Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("По строке " + arg + " не удалось определить число");
        }
        return longArg;
    }
    /**
     * Вычисление суммы двух целых чисел
     * @param arg1 число 1 задается строкой
     * @param arg2 число 2 задается строкой
     * @return Сумма
     * @throws IllegalArgumentException если из строк не удается получить числа
     */
    public long calcSum(String arg1, String arg2) throws IllegalArgumentException {
        return (convertStringToInteger(arg1)+convertStringToInteger(arg2));
    }

    /**
     * Вычислить часть факториала
     * @param begin начаьт с этого числа
     * @param factor шагать на эточ исло
     * @param argument закончить не позднее этого числа
     */
    public BigInteger calcPartOfFactorial(Long begin,Long factor,Long argument) {
        BigInteger result=new BigInteger("1");
        for(long i = begin;i<=argument;i+=factor) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
    /**
     * Посчитать факториал
     * @param arg число в виде строки от которого считать факториал
     * @return значение факториала
     * @throws IllegalArgumentException если из строки не удается получить число или если число меньше нуля или
     * если результат не удается поместить в переменную типа long
     */
    public BigInteger calcFactorial(String arg, int threadsCount) throws IllegalArgumentException, InterruptedException, ExecutionException {
        long intArg = convertStringToInteger(arg);
        if (intArg<0) {
            throw new IllegalArgumentException(arg + " должен быть больше 0 или 0");
        }
        if(threadsCount<1){
            throw new IllegalArgumentException("threadsCount="+Integer.valueOf(threadsCount).toString()+" должен быть больше 0 или 0");
        }
        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);
        BigInteger factorialValue = new BigInteger("1");
        try {
            ArrayList<Callable<BigInteger>> tasks = new ArrayList<>();
            for (int i = 0; i < threadsCount; i++) {
                final long begin = i + 1;
                tasks.add(() -> calcPartOfFactorial(begin, (long) threadsCount, intArg));
            }
            List<Future<BigInteger>> results = executor.invokeAll(tasks);
            for (int i = 0; i < threadsCount; i++) {
                factorialValue = factorialValue.multiply(results.get(i).get());
            }
        } finally {
            executor.shutdown();
        }
        return factorialValue;
    }

    /**
     * Рассчитать числа Фиббоначи не большие чем аргумент
     * @param longArg аргумент
     * @return ArrayList<Long> с числами Фиббоначи
     */
    public ArrayList<Long> calcFibonacci(long longArg){
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(0, 1L);
        arrayList.add(1, 1L);
        int index = 2;
        long indexFib = 2L;
        while(indexFib<=longArg){
            arrayList.add(index,indexFib);
            index++;
            indexFib=arrayList.get(index-1)+arrayList.get(index-2);
        }
        return arrayList;
    }
    /**
     * Раскладывает неотрицательное целое число на сумму чисел Фибоначчи
     * @param arg неотрицательное целое число
     * @return массив long чисел Фибоначчи которые должны давать в сумме arg
     * @throws IllegalArgumentException если arg не удается преобразовать в целое число или arg меньше 0 или arg
     * не удается представить в виде суммы чисел из ряда Фибоначчи (видимо это только 0)
     */
    public long[] decomposeFibonacci(String arg) throws IllegalArgumentException {
        long longArg = convertStringToLong(arg);
        if (longArg<0) {
            throw new IllegalArgumentException(arg + " должен быть больше либо равен 0");
        }
        ArrayList<Long> arrayList =  calcFibonacci(longArg);
        ArrayList<Long> resultList =  new ArrayList<>();

        long tekArg = longArg;
        for(int index = arrayList.size()-1;tekArg>0 && index>0;index--){
            if(arrayList.get(index)<=tekArg){
                resultList.add(arrayList.get(index));
                tekArg=tekArg-arrayList.get(index);
            }
        }
        if(tekArg>0 || resultList.size()==0){
            throw new IllegalArgumentException(arg + " не удалось представить как сумму чисел Фибоначчи остаток "+tekArg);
        }
        long[] result = new long[resultList.size()];
        for (Long a : resultList) {
            result[resultList.indexOf(a)] = a;
        }
        return result;
    }
}
