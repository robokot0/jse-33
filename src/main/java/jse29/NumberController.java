package jse29;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class NumberController {
    /**
     * Сервисный класс
     */
    NumberService numberService;
    /**
     * объект для ввода комманд
     */
    protected final Scanner scanner = new Scanner(System.in);

    /**
     * Конструктор
     * @param numberService ссылка на сервис
     */
    public NumberController(NumberService numberService) {
        this.numberService = numberService;
    }

    /**
     * Ввод строкового параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String enterStringCommandParameter(final String parameterName){
        System.out.println("Введите "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        if (parametervalue.isEmpty())
            return null;
        return parametervalue;
    }
    /**
     * Вычисление суммы двух целых чисел
     * @throws IllegalArgumentException неправильный аргумент
     */
    public void calcSum() throws IllegalArgumentException {
        System.out.println("[Вычисление суммы двух целых чисел]");
        final String arg1 = enterStringCommandParameter("первый аргумент");
        if(arg1==null) throw new IllegalArgumentException("Ошибка аргумент 1 не задан");
        final String arg2 = enterStringCommandParameter("второй аргумент");
        if(arg2==null) throw new IllegalArgumentException("Ошибка аргумент 2 не задан");
        System.out.println("Сумма: " + numberService.calcSum(arg1, arg2));
        System.out.println("[ОК]");
    }
    /**
     * Вычисление факториала неотрицательного целого числа
     * @throws IllegalArgumentException  неправильный аргумент
     */
    public void calcFactorial() throws IllegalArgumentException, InterruptedException, ExecutionException {
        System.out.println("[Вычисление факториала неотрицательного целого числа]");
        final String arg = enterStringCommandParameter("неотрицательное целое число");
        if(arg==null) throw new IllegalArgumentException("Ошибка аргумент не задан");
        final String threadCount = enterStringCommandParameter("число потоков");
        if(threadCount==null) throw new IllegalArgumentException("число потоков не задано");
        final int intThreadCount = Integer.parseInt(threadCount);
        long startTime = System.currentTimeMillis();
        System.out.println("Факториал: " + numberService.calcFactorial(arg,intThreadCount));
        long current = System.currentTimeMillis();
        System.out.println("[ОК]" + Long.valueOf(current-startTime).toString() + " ms");
    }
    /**
     * Разложение на сумму чисел Фибоначи
     * @throws IllegalArgumentException  неправильный аргумент
     */
    public void decomposeFibonacci() throws IllegalArgumentException {
        System.out.println("[Разложение неотрицательное целого числа на сумму чисел Фибоначчи]");
        final String arg = enterStringCommandParameter("неотрицательное целое число");
        if(arg==null) throw new IllegalArgumentException("Ошибка аргумент не задан");
        long[] result = numberService.decomposeFibonacci(arg);
        System.out.println("Разложение:");
        for (long l : result) {
            System.out.println(l);
        }
        System.out.println("[ОК]");
    }
    /**
     * Показать справку
     */
    public void displayHelp()  {
        System.out.println("sum - Сумма двух целых чисел");
        System.out.println("fact - Факториал числа");
        System.out.println("fib - Разложение по числам Фибоначи");
        System.out.println("exit - Выход");
        System.out.println("help - Справка");
    }
    /**
     * Показать приглашешние
     */
    public void displayWelcome()  {
        System.out.println("Введите комманду");
    }
}
