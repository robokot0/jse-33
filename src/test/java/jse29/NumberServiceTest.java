package jse29;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class NumberServiceTest {
    /**
     * Тестируемый сервис
     */
    private NumberService numberService = null;

    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());

    /**
     * Создание сервиса до всех тестов
     */
    @BeforeEach
    void numberServiceInit() {
        numberService = new NumberService();
    }
    /**
     * Проверка преобразования строки в число на небольшом числе
     */
    @Test
    void convertStringToInteger1PositivValue() throws IllegalArgumentException {
        assertEquals(Integer.valueOf(4),numberService.convertStringToInteger("4"));
    }
    /**
     * Проверка преобразования строки в число на отрицательном числе
     */
    @Test
    void convertStringToIntegerNegativValue() throws IllegalArgumentException {
        assertEquals(Integer.valueOf(-4),numberService.convertStringToInteger("-4"));
    }
    /**
     * Проверка преобразования не числа
     */
    @Test
    void convertStringToIntegerNotNumberArg() throws IllegalArgumentException {
        assertThrows(IllegalArgumentException.class,() -> numberService.convertStringToInteger("aaaa"));
    }

    @Test
    void convertStringToLong1PositivValue() throws IllegalArgumentException {
        assertEquals(Long.valueOf(4),numberService.convertStringToLong("4"));
    }
    /**
     * Проверка преобразования строки в число на отрицательном числе
     */
    @Test
    void convertStringToLongNegativValue() throws IllegalArgumentException {
        assertEquals(-4L,numberService.convertStringToLong("-4"));
    }
    /**
     * Проверка преобразования не числа
     */
    @Test
    void convertStringToLongNotNumberArg() throws IllegalArgumentException {
        assertThrows(IllegalArgumentException.class,() -> numberService.convertStringToLong("aaaa"));
    }

    /**
     * Проверка суммирвоания двух небольших чисел
     */
    @Test
    void calcSum() throws IllegalArgumentException {
        assertEquals(4,numberService.calcSum("1","3"));
    }

    /**
     * Проверка первый аругмент sum не число
     */
    @Test
    void sumExceptionArg1NotNumber(){
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum("aaa","111"));
    }
    /**
     * Проверка второй аругмент sum не число
     */
    @Test
    void sumExceptionArg2NotNumber(){
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum("111","фффф"));
    }
    /**
     * Проверка ошибка переполнения при суммирвоании Long.MAX_VALUE
     */
    @Test
    void sumExceptionOverflowMaxLong(){
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum(Long.valueOf(Long.MAX_VALUE).toString() ,"10"));
    }
    /**
     * Проверка ошибка переполнения при суммирвоании Long.MIN_VALUE
     */
    @Test
    void sumExceptionOverflowMinLong(){
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum(Long.valueOf(Long.MIN_VALUE).toString() ,"-10"));
    }
    /**
     * Проверка ошибка при попытке вычислить факториал ототрицательного числа
     */
    @Test
    void factorialNegativeValue(){
        assertThrows(IllegalArgumentException.class,() -> numberService.calcFactorial("-10",1));
    }
    /**
     * Проверка ошибка при попытке вычислить факториал небольшого числа
     */
    @Test
    void factorial() throws IllegalArgumentException, InterruptedException, ExecutionException {
        assertEquals(BigInteger.valueOf(2432902008176640000L),numberService.calcFactorial("20",1));
    }
    /**
     * Проверка число потоков должно быть больше 0
     */
    @Test
    void factorialThreadsCountg1(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcFactorial("10",0));
    }
    /**
     * Проверка вычисления факториала небольшого числа в одну нить
     */
    @Test
    void factorial20() throws IllegalArgumentException, InterruptedException, ExecutionException {
        NumberService numberService = new NumberService();
        assertEquals("2432902008176640000",numberService.calcFactorial("20",1).toString());
    }

    /**
     * Проверка вычисления факториала небольшого числа в пять нитей
     */
    @Test
    void factorial20thread5() throws IllegalArgumentException, InterruptedException, ExecutionException {
        NumberService numberService = new NumberService();
        assertEquals("2432902008176640000",numberService.calcFactorial("20",5).toString());
    }
    /**
     * Проверка вычисления факториала большого числа в пять нитей
     */
    @Test
    void factorial500thread5() throws IllegalArgumentException, InterruptedException, ExecutionException {
        NumberService numberService = new NumberService();
        assertEquals("1220136825991110068701238785423046926253574342803192842192413588385845373153881997605496447502203281863013616477148203584163378722078177200480785205159329285477907571939330603772960859086270429174547882424912726344305670173270769461062802310452644218878789465754777149863494367781037644274033827365397471386477878495438489595537537990423241061271326984327745715546309977202781014561081188373709531016356324432987029563896628911658974769572087926928871281780070265174507768410719624390394322536422605234945850129918571501248706961568141625359056693423813008856249246891564126775654481886506593847951775360894005745238940335798476363944905313062323749066445048824665075946735862074637925184200459369692981022263971952597190945217823331756934581508552332820762820023402626907898342451712006207714640979456116127629145951237229913340169552363850942885592018727433795173014586357570828355780158735432768888680120399882384702151467605445407663535984174430480128938313896881639487469658817504506926365338175055478128640000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",numberService.calcFactorial("500",5).toString());
    }
    /**
     * Факториал 0
     */
    @Test
    void factorial0() throws IllegalArgumentException, InterruptedException, ExecutionException {
        assertEquals(BigInteger.valueOf(1L),numberService.calcFactorial("0",1));
    }

    /**
     * Факториал 1
     */
    @Test
    void factorial1() throws IllegalArgumentException, InterruptedException, ExecutionException {
        assertEquals(BigInteger.valueOf(1L),numberService.calcFactorial("1",1));
    }

    /**
     * Факториал 2
     */
    @Test
    void factorial2() throws IllegalArgumentException, InterruptedException, ExecutionException {
        assertEquals(BigInteger.valueOf(2L),numberService.calcFactorial("2",1));
    }

    /**
     * Факториал 4
     */
    @Test
    void factorial4() throws IllegalArgumentException, InterruptedException, ExecutionException {
        assertEquals(BigInteger.valueOf(24L),numberService.calcFactorial("4",1));
    }

    /**
     * Факториал не числовой аргумент
     */
    @Test
    void fibonacciNoNumberArg(){
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("aaa"));
    }
    /**
     * Разложение на числа Фиббоначи ошибка на отрицательный аргумент
     */
    @Test
    void fibonacciNegativeArg(){
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("-1"));
    }
    /**
     * Разложение 0 на числа Фиббоначи ошибка
     */
    @Test
    void fibonacciZeroArg(){
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("0"));
    }

    /**
     * Разложение небольшого числа на слагаемые проверка что сумма слагаемых осталась равна числу
     */
    @Test
    void decomposeFibonacci() throws IllegalArgumentException {
        String strResult = "8252";

        long[] result = numberService.decomposeFibonacci(strResult);

        long sumResult = 0L;
        for (long l : result) {
            logger.info(l);
            sumResult = sumResult + l;
        }

        assertEquals(Long.valueOf(strResult).longValue(),sumResult);
    }

    /**
     * Расчет списка чисел Фибоначи
     * в списке должен быть ряд Фибоначи
     */
    @Test
    void calcFibonacci() throws IllegalArgumentException {

        long longArg = 150;

        ArrayList<Long> resultForCheck = numberService.calcFibonacci(longArg);

        assertEquals(1L,resultForCheck.get(0).longValue());
        assertEquals(1L,resultForCheck.get(1).longValue());

        for(int i=2;i<resultForCheck.size();i++) {
            assertEquals(resultForCheck.get(i - 1) + resultForCheck.get(i - 2),resultForCheck.get(i).longValue());
        }

        logger.info(resultForCheck);
    }
    /**
     * Расчет списка чисел Фибоначи
     * последний элемент должен быть меньшге или равен аргументу
     */
    @Test
    void calcFibonacciLastMemberLeArg() throws IllegalArgumentException {

        long longArg = 150;

        ArrayList<Long> resultForCheck = numberService.calcFibonacci(longArg);

        assertTrue(resultForCheck.get(resultForCheck.size() - 1) <=longArg);
    }
}